#include <sys/types.h>         
#include <sys/stat.h>
#include <stdio.h>             
#include <stdlib.h>            
#include <stddef.h>             
#include <string.h>           
#include <unistd.h>             
#include <signal.h>         
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

#define LIMITE 100
#define TAMANO 200 

int conectar(int domain,int type,int protocol, const struct sockaddr *addr, socklen_t alen){
	
	int numsec, fd; 

	for (numsec = 1; numsec <= LIMITE; numsec <<= 1) { 

		if (( fd = socket(domain, type, protocol)) < 0) {
			return(-1); 
		}

		if (connect(fd, addr, alen) == 0) {
			return(fd); 
		} 
		close(fd); 				

		if (numsec <= LIMITE/2){
			sleep(numsec); 
		}
	} 

	return(-1); 
}

int main( int argc, char *argv[]) { 

	int sockfd;

	if(argc == 1){
		printf("Uso: ./cliente <ip> <puerto> <string>\n");
		exit(-1);
	}

	if(argc != 4){
		printf( "Cantidad de elementos erronea\n");
	}

	int puerto = atoi(argv[2]); //Convertir el puerto a entero
	//printf("%d\n",puerto);


	//Direccion del servidor
	struct sockaddr_in direccion_cliente;

	memset(&direccion_cliente, 0, sizeof(direccion_cliente));	

	direccion_cliente.sin_family = AF_INET;		//IPv4
	direccion_cliente.sin_port = htons(puerto);		
	direccion_cliente.sin_addr.s_addr = inet_addr(argv[1]) ;	

	if (( sockfd = conectar( direccion_cliente.sin_family, SOCK_STREAM, 0, (struct sockaddr *)&direccion_cliente, sizeof(direccion_cliente))) < 0) { 
		printf("Falló conexión\n"); 
		exit(-1);
	} 
	
	//Enviar al servidor	
	int p = 0;
	p = send( sockfd, argv[3] , strlen(argv[3]), 0); 	
	if (p < 0) {
		printf("Send error Cliente\n"); 
	}

	//Respuesta del servidor
	int n = 0;
	char buf[TAMANO] =  {0};
	while (( n = recv( sockfd, buf, TAMANO, 0)) > 0) {				
		write( STDOUT_FILENO, buf, n); 			
	}
	if (n < 0) 	
		printf("Recv error Cliente\n"); 

	printf("\n");


	return 0; 
}
