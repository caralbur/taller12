#include <sys/types.h>          
#include <sys/stat.h>
#include <stdio.h>              
#include <stdlib.h>            
#include <stddef.h>             
#include <string.h>            
#include <unistd.h>           
#include <signal.h>           
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <ctype.h>

#define BUFLEN 128 
#define QLEN 10 

#ifndef HOST_NAME_MAX 
#define HOST_NAME_MAX 256 
#endif	
#define TAMANO 200 

//Funcion de ayuda para setear la bandera close on exec
void set_cloexec(int fd){
	if(fcntl(fd, F_SETFD, fcntl(fd, F_GETFD) | FD_CLOEXEC) < 0){
		printf("error al establecer la bandera FD_CLOEXEC\n");	
	}
}


//Funcion para inicializar el servidor
int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen){

	int fd;
	int err = 0;
	
	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;
		
	if(bind(fd, addr, alen) < 0)
		goto errout;
		
	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){
		
			if(listen(fd, qlen) < 0)
				goto errout;
	}
	return fd;


errout:
	err = errno;
	close(fd);
	errno = err;
	return (-1);
}


int main( int argc, char *argv[]) { 
	int sockfd;

	if(argc == 1){
		printf("Uso: ./cliente <ip> <puerto>\n");
		exit(-1);
	}

	if(argc != 3){
		printf( "Cantidad de elementos erronea\n");
	}


	int puerto = atoi(argv[2]);
	

	//Direccion del servidor
	struct sockaddr_in direccion_servidor;

	memset(&direccion_servidor, 0, sizeof(direccion_servidor));	

	direccion_servidor.sin_family = AF_INET;		//IPv4
	direccion_servidor.sin_port = htons(puerto);		
	direccion_servidor.sin_addr.s_addr = inet_addr(argv[1]) ;	

	//inicalizamos servidor (AF_INET + SOCK_STREAM = TCP)
	if( (sockfd = initserver(SOCK_STREAM, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor), 1000)) < 0){	//Hasta 1000 solicitudes en cola 
		printf("Error al inicializar el servidor\n");	
	}
	
	int clfd = accept(sockfd, NULL, NULL);
	
	//Respuesta del cliente		
	int n = 0;
	char buf[TAMANO] = {0};
	n = recv( clfd, buf, TAMANO, 0);
	if (n < 0) {	
		perror("Recv error Servidor\n"); 
	}
	printf("Procesando...\n");

	//Cambiar a mayusculas
	for (int i=0;i<n; i++){
		buf[i]=toupper(buf[i]);
	}
	
	//Enviar al cliente
	while(1){		
		send( clfd, buf , 100, 0); 
		close(clfd);
	}

	exit( 1); 
}

